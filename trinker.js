const { includes, map } = require("./people");

module.exports = {
  title: function () {
    return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow;
  },

  line: function (title = "=") {
    return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black;
  },

  allMale: function (p) {
    function tousLesHommes(male) {
      return male.gender == "Male";
    }
    return p.filter(tousLesHommes);
  },

  allFemale: function (p) {
    function toutesLesFemmes(female) {
      return female.gender == "Female";
    }
    return p.filter(toutesLesFemmes);
  },

  nbOfMale: function (p) {
    return this.allMale(p).length;
  },

  nbOfFemale: function (p) {
    return this.allFemale(p).length;
  },

  nbOfMaleInterest: function (p) {
    function lookingFor(male) {
      return male.looking_for == "M";
    }
    return p.filter(lookingFor).length;
  },

  nbOfFemaleInterest: function (p) {
    function lookingFor(female) {
      return female.looking_for == "F";
    }
    return p.filter(lookingFor).length;
  },

  nbOfIncomeTwoThousand: function (p) {
    function income(pay) {
      return parseInt(pay.income.substring(1)) > 2000;
    }
    return p.filter(income).length;
  },

  nbPeopleLikeDrama(p) {
    function people(like) {
      return like.pref_movie.includes("Drama");
    }
    return p.filter(people).length;
  },

  nbWomenLikeSF(p) {
    function women(scifi) {
      return scifi.pref_movie.includes("Sci-Fi");
    }
    return this.allFemale(p).filter(women).length;
  },


  nbPeopleLikeDoc(p) {
    function all(doc) {
      return (
        doc.pref_movie.includes("Documentary") &&
        parseInt(doc.income.substring(1)) > 1482
      );
    }
    return p.filter(all).length;
  },



  liPeopleIncomeFourThousand(p) {
    const list = p.filter(function (e) {
      const money = parseInt(e.income.substring(1));
      const comp = money > 4000;
      return comp;
    });
    return list.map(
      (el) =>
        `First name :${el.first_name}, last name :${el.last_name}, salary: ${el.income}, id :${el.id} `
    );
  },



  richestMan(p) {
    return this.allMale(p)
      .sort(function (a, b) {
        return (
          parseInt(b.income.substring(1)) - parseInt(a.income.substring(1))
        );
      })
      .map(
        (el) =>
          `Richest man: ${el.first_name}, ${el.last_name} with id: ${el.id}`
      )[0];
  },



  averageSalary(p) {
    sum = 0;
    for (let x of p) {
      sum += parseFloat(x.income.substring(1));
    }
    return (sum / p.length).toFixed(2);
  },



  medianSalary(p) {
    let orderedNumbers = p.sort(
      (a, b) =>
        parseFloat(a.income.substring(1)) - parseFloat(b.income.substring(1))
    );
    return (
      (parseFloat(orderedNumbers[p.length / 2].income.substring(1)) +
        parseFloat(orderedNumbers[p.length / 2 - 1].income.substring(1))) /
      2.0
    );
  },



  nbPersonNorth(p) {
    nbPeopleNorth = 0;
    for (var i = 0; i < p.length; i++) {
      if (p[i].latitude > 0) {
        nbPeopleNorth += 1;
      }
    }
    return nbPeopleNorth;
  },



  averageSalaryPeopleSouth(p) {
    let nbPeopleSouth = 0;
    let totalSalary = 0;
    for (var i = 0; i < p.length; i++) {
      if (p[i].latitude < 0) {
        nbPeopleSouth += 1;
        totalSalary += parseFloat(p[i].income.substring(1));
      }
    }
    return (totalSalary / nbPeopleSouth).toFixed(2);
  },



  nearestPersonBerenice(p) {
    let latitudeBerenice = 0;
    let longitudeBerenice = 0;
    let shortestDistance = null;
    let nom = "";
    let id = "";
    let idBerenice = "";
    for (let i = 0; i < p.length; i++) {
      if (p[i].first_name == "Bérénice" && p[i].last_name == "Cawt") {
        latitudeBerenice = p[i].latitude;
        longitudeBerenice = p[i].longitude;
        idBerenice = p[i].id;
      }
    }
    for (let i = 0; i < p.length; i++) {
      let distance = getDistanceFromLatLonInKm(
        latitudeBerenice,
        longitudeBerenice,
        p[i].latitude,
        p[i].longitude
      );
      if (
        idBerenice != p[i].id &&
        (shortestDistance == null || distance < shortestDistance)
      ) {
        shortestDistance = distance;
        nom = p[i].first_name + " " + p[i].last_name;
        id = p[i].id;
      }
    }
    return nom + " " + id;
  },


  nearestPersonRui(p) {
    let latitudeRui = 0;
    let longitudeRui = 0;
    let shortestDistance = null;
    let nom = "";
    let id = "";
    let idRui = "";
    for (let i = 0; i < p.length; i++) {
      if (p[i].first_name == "Ruì" && p[i].last_name == "Brach") {
        latitudeRui = p[i].latitude;
        longitudeRui = p[i].longitude;
        idRui = p[i].id;
      }
    }
    for (let i = 0; i < p.length; i++) {
      let distance = getDistanceFromLatLonInKm(
        latitudeRui,
        longitudeRui,
        p[i].latitude,
        p[i].longitude
      );
      if (
        idRui != p[i].id &&
        (shortestDistance == null || distance < shortestDistance)
      ) {
        shortestDistance = distance;
        nom = p[i].first_name + " " + p[i].last_name;
        id = p[i].id;
      }
    }
    return nom + " " + id;
  },


  nearestTenPersonsJosee(p) {
    let latitudeJose = 0;
    let longitudeJose = 0;
    let array = [];
    let arrayTen = [];
    let nom = "";
    for (let i = 0; i < p.length; i++) {
      if (p[i].first_name == "Josée" && p[i].last_name == "Boshard") {
        latitudeJose = p[i].latitude;
        longitudeJose = p[i].longitude;
        idJose = p[i].id;
      }
    }
    for (let i = 0; i < p.length; i++) {
      var distance = getDistanceFromLatLonInKm(
        latitudeJose,
        longitudeJose,
        p[i].latitude,
        p[i].longitude
      );
      array.push({
        distance: distance,
        firstname: p[i].first_name,
        lastname: p[i].last_name,
        id: p[i].id,
      });
    }
    //end for

    const distanceOreder = array.sort(function (a, b) {
      return a.distance - b.distance;
    });

    for (let i = 1; i <= 10; i++) {
      const tenPeople = distanceOreder[i];
      nom = tenPeople.firstname + " " + tenPeople.lastname + " " + tenPeople.id;
      arrayTen.push(nom);
    }
    return arrayTen;
  },


  peopleWorkingGoogle(p) {
    let array = [];
    for (let i = 0; i < p.length; i++) {
      if (p[i].email.includes("@google")) {
        array.push({
          firstname: p[i].first_name,
          lastname: p[i].last_name,
          id: p[i].id,
        });
      }
    }
    return array;
  },


  theOldestPerson(p) {
    const sortedAge = p.sort(
      (a, b) => Date.parse(a.date_of_birth) - Date.parse(b.date_of_birth)
    )[0];
    return `${sortedAge.first_name}, ${sortedAge.last_name}, ${sortedAge.id}`;
  },


  theYoungestPerson(p) {
    const sortedAge = p.sort(
      (a, b) => Date.parse(b.date_of_birth) - Date.parse(a.date_of_birth)
    )[0];
    return `${sortedAge.first_name}, ${sortedAge.last_name}, ${sortedAge.id}`;
  },


  averageAge(p) {
    sum = 0;
    today_date = new Date();
    today_year = today_date.getFullYear();
    for (x of p) {
      sum += today_year - parseInt(x.date_of_birth)
    }
    return (sum / p.length).toFixed(2);
  },

  

  match: function (p) {
    return "not implemented".red;
  },
};



function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI / 180);
}
